# Java Client examples

Here you can find example usages of the Java Client in two Java files, **RunClient** and **RunTokenHandler**.

#### RunClient.java
shows how to use Java Client in order to obtain authentication token from OpenStack Keystone.

#### RunTokenHandler.java
shows how to extract username and unique identifier of the Keystone user from Keystone authentication token.

> Note: running this procedure doesn't require you to have a Keystone instance up and running. All you need is a valid Keystone token, retrieved in an arbitrary manner.

## Compile and run
Assuming you have installed Java project according to installation instructions in keystone-clients/java-wrappers/, then `mvn install` command created a **.jar** inside your local maven repository (~/.m2/repository/witdom/openstack/si/witdom-openstack-tools) with ending *-jar-with-dependencies.jar*.
To use Java snippets like the ones in *RunClient* and *RunTokenHandler* in your code (which does not have to be built with maven), you must add this jar to Java classpath.

One way to do this is to edit your CLASSPATH environment variable. Another is to add it to Java classpath when compiling and running code like this:

* Compilation:
    ````
    javac -cp "/path/to/your/local/jar-with-dependencies.jar:." RunClient.java
    ````

* Running the code:
    ````
    java  -cp "/path/to/your/local/jar-with-dependencies.jar:." RunClient
    # Runs client example, which retrieves authentication token from Keystone and prints it to console
    ````

The same procedure can be used to compile and run *RunTokenHandler*.