import witdom.openstack.si.client.Client;
import witdom.openstack.si.config.KeystoneConfiguration;

/* RunClient.java
 * Demmonstrates how Java components will retrieve authentication token from Keystone
 * Forwarding retrieved authentication token as a HTTP header to broker requests is omitted here
 */
public class RunClient
{
    public static void main (String[] args)
    {
	    /* Define endpoints for Keystone and broker */
        // This configuration must match with Keystone setup
        KeystoneConfiguration keystoneConf = new KeystoneConfiguration("172.16.95.136");

        // Initialize client
        Client client = new Client(keystoneConf);

        // Retrieve authentication token
        String authToken = client.authenticateWithUsernamePassword("testuser", "testuser");

        System.out.println("Retrieved authentication token " + authToken);
    }
}
