import witdom.openstack.si.client.TokenHandler;

/* RunTokenHandler.java
 * Demmonstrates how user data is extracted from a valid Keystone token and what happens if the token is invalid (not in the correct format)
 * Basically, you have to create an instance of the TokenHandler class and pass the token to its constructor.
 *      Then, you can call its 'getUserId()' or 'getUserName()' methods.
 */
public class RunTokenHandler
{
    public static void main(String[] args)
    {
        /* token should be the value of 'X-Auth-Token' header provided by Keystone */
        /* It must be a valid Keystone token, otherwise NullPointerException will be thrown */
        String validToken = "MIIH+AYJKoZIhvcNAQcCoIIH6TCCB+UCAQExDTALBglghkgBZQMEAgEwggZGBgkqhkiG9w0BBwGgggY3BIIGM3sidG9rZW4iOiB7Im1ldGhvZHMiOiBbInBhc3N3b3JkIl0sICJyb2xlcyI6IFt7ImlkIjogImZlOTdiZTYwOGQ3OTQ2ZmI4ZTg4NTMzMzg2NTAxNDQxIiwgIm5hbWUiOiAiYWRtaW4ifV0sICJleHBpcmVzX2F0IjogIjIwMTctMDMtMzFUMDY6NDc6MjUuMzUwMTE0WiIsICJwcm9qZWN0IjogeyJkb21haW4iOiB7ImlkIjogImRlZmF1bHQiLCAibmFtZSI6ICJEZWZhdWx0In0sICJpZCI6ICI3Yzg3ZjBmNzNhMjg0ZWFjOWFlYTJkYjk3ZmMzNmY3NiIsICJuYW1lIjogImFkbWluIn0sICJjYXRhbG9nIjogW3siZW5kcG9pbnRzIjogW3sidXJsIjogImh0dHA6Ly9sb2NhbGhvc3Q6MzUzNTcvdjMiLCAicmVnaW9uIjogIlJlZ2lvbk9uZSIsICJpbnRlcmZhY2UiOiAiYWRtaW4iLCAiaWQiOiAiMGQwZjVkYzE1NzY2NDVmYTkzMWE2MWZkMzMxMGUzZDAifSwgeyJ1cmwiOiAiaHR0cDovL2xvY2FsaG9zdDo1MDAwL3YzIiwgInJlZ2lvbiI6ICJSZWdpb25PbmUiLCAiaW50ZXJmYWNlIjogInB1YmxpYyIsICJpZCI6ICI5YmJkOGNhMTRjZDE0MjU0YmU4YjcwNzZmNjU1MTk2ZSJ9XSwgInR5cGUiOiAiaWRlbnRpdHkiLCAiaWQiOiAiMWM1NzI0NWZjMGQ0NDA4ZTkwZTdmN2ExZmNlYWI3NzgiLCAibmFtZSI6ICJrZXlzdG9uZSJ9LCB7ImVuZHBvaW50cyI6IFt7InVybCI6ICJodHRwOi8vbG9jYWxob3N0OjgwODAvdjEvQVVUSF83Yzg3ZjBmNzNhMjg0ZWFjOWFlYTJkYjk3ZmMzNmY3NiIsICJyZWdpb24iOiAiUmVnaW9uT25lIiwgImludGVyZmFjZSI6ICJpbnRlcm5hbCIsICJpZCI6ICIxMzNkMzM2OWJhNTE0MGJhYTMxMGFkMDUyOTY5OWI2MCJ9LCB7InVybCI6ICJodHRwOi8vbG9jYWxob3N0OjgwODAvdjEvQVVUSF83Yzg3ZjBmNzNhMjg0ZWFjOWFlYTJkYjk3ZmMzNmY3NiIsICJyZWdpb24iOiAiUmVnaW9uT25lIiwgImludGVyZmFjZSI6ICJwdWJsaWMiLCAiaWQiOiAiNmM1NTcwMWE0NjU1NDdhOTgzM2Y1YTRjOGUxMjUzYjQifSwgeyJ1cmwiOiAiaHR0cDovL2xvY2FsaG9zdDo4MDgwLyIsICJyZWdpb24iOiAiUmVnaW9uT25lIiwgImludGVyZmFjZSI6ICJhZG1pbiIsICJpZCI6ICI4OGFlYmVhMzM3ZWE0ZTg0OTAxNmMyZDhlNGYxZGIwMiJ9XSwgInR5cGUiOiAib2JqZWN0LXN0b3JlIiwgImlkIjogIjQ0MmFkNDk1YTYxNzRiM2ZiYzkwODVhZDJmZTg1Nzk4IiwgIm5hbWUiOiAic3dpZnQifSwgeyJlbmRwb2ludHMiOiBbeyJ1cmwiOiAiaHR0cDovL2xvY2FsaG9zdDo5MzExL3YxIiwgInJlZ2lvbiI6ICJSZWdpb25PbmUiLCAiaW50ZXJmYWNlIjogInB1YmxpYyIsICJpZCI6ICI2ODIzYzlkMDQzM2Q0NThiOTQzMGVjZjA1M2QwY2ViNyJ9XSwgInR5cGUiOiAia2V5LW1hbmFnZXIiLCAiaWQiOiAiNDdjM2YzZGVmMGRiNGQzYWIwYjRlM2NmMDE4MDBlM2YiLCAibmFtZSI6ICJiYXJiaWNhbiJ9XSwgImV4dHJhcyI6IHt9LCAidXNlciI6IHsiZG9tYWluIjogeyJpZCI6ICJkZWZhdWx0IiwgIm5hbWUiOiAiRGVmYXVsdCJ9LCAiaWQiOiAiODI2MmYzZTcyNTFkNGM1MDg0MTM4OWQwMWZmNDc3YjUiLCAibmFtZSI6ICJhZG1pbiJ9LCAiaXNzdWVkX2F0IjogIjIwMTctMDMtMzFUMDU6NDc6MjUuMzUwMTQ2WiJ9fTGCAYUwggGBAgEBMFwwVzELMAkGA1UEBhMCVVMxDjAMBgNVBAgMBVVuc2V0MQ4wDAYDVQQHDAVVbnNldDEOMAwGA1UECgwFVW5zZXQxGDAWBgNVBAMMD3d3dy5leGFtcGxlLmNvbQIBATALBglghkgBZQMEAgEwDQYJKoZIhvcNAQEBBQAEggEARenNBw5Fc6hkQTnc3sHP+OvJXPYl2GOlzGJTjjScIh2eJILuUeFb14nuqaHWML9pJ8mHr-GlS3xQZIwQb99JBX3+sPz9B0msokOwpalkYy5ggJ4e+3D7cke9kjxAfXa5sHbjgTADI-t772WAP97BrCQK1ssQlxQyQadvQCGIF+UFIi-Jm6jRdxkkpD9f2QsIcv6bw4Ou5DjXBOQuVd6ePskdSj1Ui8FJLzrzDTs3+9+P11ygpeWSv7jWmLxOne5DenXU41CTvCG-ATPTgRH1UkfO78Tu7TI2SIYaeqjEHWQiQrQjbTwtCdBcGeRYH+wdmH-D3XsMQRY1fOyND3twYQ==";
        System.out.println("Example 1 - passing a token in a valid format");
        extractInfoFromValidToken(validToken);

        System.out.println("\nExample 2 - passing a token in an invalid format");
        extractInfoFromInvalidToken("Something");
    }

    public static void extractInfoFromValidToken(String token)
    {
        TokenHandler th = new TokenHandler(token);

        /* Extract and print unique Keystone user ID and user's name from the token */
        String userId = th.getUserId();
        String userName = th.getUserName();
        System.out.println("Unique ID of the token holder = " + userId);
        System.out.println("Username of the token holder = " + userName);
    }

    public static void extractInfoFromInvalidToken(String token)
    {
        TokenHandler th = new TokenHandler(token);

        try {
            th.getUserId();
        } catch (NullPointerException e) {
            System.out.println("Tried to extract info from invalid token - caught NullPointerException.");
        }
    }
}