# Example: obtaining and validating Keystone authentication tokens from JavaScript

This example demonstrates how to use [JavaScript IAM (Keystone) Client wrapper](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone-clients/javascript-wrappers/?at=master) for validating authentication tokens (and obtaining them prior to that). Token validation is performed _online_, meaning that Keystone is contacted every time a validating party wants to validate a token.

## Prerequsites
* NodeJS
* NodeJS module _openstack-token-utils_ must be installed (see installation instructions provided with the JavaScript Keystone Client wrapper)
* A running instance of OpenStack Keystone - you can build a Dockerized version based on instructions [here](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone/docker/?at=master).

## Running the example
Once your (local) Keystone instance is up and running, modify Keystone configuration in _keystone.js_ - this step is not necessarry if you are using the provided Dockerized Keystone image.

Then, you can run the example with

````bash
$ node authExample.js
```` 

which will read configuration in _keystone.js_ and run a function taking care of online token validation (just for demonstration purposes).

## Description
Script _authExample.js_ comprises two functions that demonstrate simple authentication token management:

* *validationExample1()* - Retrieves an authentication token of a non-admin user configured in Keystone, then validates it.
* *validationExample2(token)* - Validates an authentication token passed to it.

Both functions wrap **validateAuthenticationToken()** function from _TokenValidationService_, which manages retrieval of administrative authentication token, necessarry for validating tokens in Keystone - all you have to do in your own JavaScript code is to provide administrative credentials (username and password) and perform an action based on token validation result.

> **Note:** User tokens in the above description correspond to WITDOM applications' authentication tokens that will be sent to WITDOM Broker. WITDOM Broker will have administrative privileges in Keystone and will consult Keystone to determine whether an application's token is vaid or not.