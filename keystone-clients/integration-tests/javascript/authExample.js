/* JavaScript IAM (Keystone) Client wrapper - example usage
*   For testing use Dockerized Keystone setup available at https://gitlab-witdom.xlab.si/WitdomWP5/witdom-core-iam/tree/master/keystone/docker
*   that already has administrative user and a test user configured
* */

var auth = require('openstack-token-utils');
var keystoneConfig = require('./keystone.js');


authService = new auth.TokenValidationService(keystoneConfig.endpoint, keystoneConfig.admin.user, keystoneConfig.admin.pass);

/* Authentication token, forwarded to WITDOM broker by WITDOM application */
//  This value will be retrieved from a WITDOM application in a production setting
var appToken = null;


/* Retrieves an example token for a user in keystone and validates it
    User's token simulates WITDOM application's token */
function validationExample1() {
    authService.getAuthToken("testuser", "testuser", function(error, token) {
        appToken = token;
        console.log("Verifying token", appToken);
        authService.validateAuthenticationToken(appToken, function(error, tokenValid) {
            /* Do something based on a token validation result */
            printTokenValidationResults(error, tokenValid);
        });
    });
}

/* Accepts externally retrieved application token and validates it */
function validationExample2(tokenToVerify) {
    authService.validateAuthenticationToken(appToken, function(error, tokenValid) {
        /* Do something based on a token validation result */
        printTokenValidationResults(error, tokenValid);
    });
}

// tokenValid == true means token is valid
// tokenValid == false means token is not valid
// error or tokenValid == undefined means some other Keystone error occured and token validation was not performed
function printTokenValidationResults(error, tokenValid) {
    if (error) {
        console.log("Keystone error:", error.status, error.message);
    } else if (tokenValid) {
        console.log("Token is valid");
    } else {
        console.log("Token is not valid");
    }
}


/* Demonstrate validation */
validationExample1();

//appToken = "MIICyAYJKoZIhvcNAQcCoIICuTCCArUCAQExDTALBglghkgBZQMEAgEwggEWBgkqhkiG9w0BBwGgggEHBIIBA3sidG9rZW4iOiB7Imlzc3VlZF9hdCI6ICIyMDE2LTExLTAzVDEwOjA0OjQ5LjUyMTk2NloiLCAiZXh0cmFzIjoge30sICJtZXRob2RzIjogWyJwYXNzd29yZCJdLCAiZXhwaXJlc19hdCI6ICIyMDE2LTExLTAzVDExOjA0OjQ5LjUyMTk1MloiLCAidXNlciI6IHsiZG9tYWluIjogeyJpZCI6ICJkZWZhdWx0IiwgIm5hbWUiOiAiRGVmYXVsdCJ9LCAiaWQiOiAiNDFhMDM0NThhZGUzNDFhZmFhMWQwMWRjYTUzYjFlNDYiLCAibmFtZSI6ICJ0ZXN0dXNlciJ9fX0xggGFMIIBgQIBATBcMFcxCzAJBgNVBAYTAlVTMQ4wDAYDVQQIDAVVbnNldDEOMAwGA1UEBwwFVW5zZXQxDjAMBgNVBAoMBVVuc2V0MRgwFgYDVQQDDA93d3cuZXhhbXBsZS5jb20CAQEwCwYJYIZIAWUDBAIBMA0GCSqGSIb3DQEBAQUABIIBAEhpASH11IG+NmMjQjw5ZxOBFWidbs3+SuJqnXWBwA6hFeN4NvCB1x8NS98GMS8LBCzgUhuhNIudowzF3YYSSC7p2RsfS3+v+WUT3y48NTGtbPdF9Z6Xs7w2VOdwLhbHNcg1qYNpceWMNIiblI2CVmy1OpdTOyxA11NPfsp0TqI5knSPL71uWkyrlrtUCdyVLRzhkqkrC5CjAjqI3jgsXGiPdck2GRERUNGp+fQmqoqJza0ZOpGdreD-T6hYjJcOFS0JHNYchhD4uDmGER+BfuNOtUT2y+a0cQHlD0TlR+abv8VZq+T3HYZZMZHl37dtA89t5bZ1TwQAj5F0odQtf20=";
//Replace the above 'appToken' value with a real token that gets printed during the execution of validationExample1()
//  or provide some arbitrary value to see that false tokens are not validated;
//validationExample2(appToken);