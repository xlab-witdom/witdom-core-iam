/* Keystone admin credentials and endpoint */
module.exports = {
	admin: {
		user: "admin",
		pass: "adminpw"
	},
	endpoint: "http://127.0.0.1:5000/v3"
};