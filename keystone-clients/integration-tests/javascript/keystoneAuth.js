//npm install https://github.com/godaddy/node-openstack-wrapper
//npm install git+https://git@github.com/godaddy/node-openstack-wrapper.git

//Before any other calls are made into the Openstack system,
protocol = "http";
keystone_url = "172.16.95.136";
keystone_port = 5000;
keystone_endpoint = protocol + "://" + keystone_url + ":" + keystone_port + "/v3";

console.log("Using Keystone service at", keystone_endpoint);

var OSWrap = require('openstack-wrapper');
var keystone = new OSWrap.Keystone(keystone_endpoint);

var adminToken = null;
var userToken = null;

//adminToken = getAdminToken();
//userToken = getTestUserToken();

//console.log("ADMIN TOKEN: ", adminToken);
//console.log("USER TOKEN: ", userToken);

function main() {
    
}

function getAdminToken() {
    getAuthToken("admin", "adminpw", function(token) {
        console.log(token);
        return token;  
    });
}

function getTestUserToken() {
    getAuthToken("testuser", "testuser", function(token) {
        console.log(token);
        return token;  
    });
}

function verifyUserToken(token) {
    
}

// Broker must have a token with admin privileges to perform token verification
    

// Validates authentication token
function verifyApplicationToken(token, offline){
    if (offline) {
        console.log("Starting offline verification");
    } else {
        console.log("Starting online verification");
    }
}

function getAuthToken(username, pw, fn) {
 keystone.getToken(username, pw, function(error, token) {
        if(error) {
            console.error('An error occured while retrieving token', error);
        } else {
            fn(token.token);
        }
    });   
}

