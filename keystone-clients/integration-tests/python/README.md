# Examples for interacting with Keystone from Python
This document describes prerequisites, instructions and additional information for running simple Keystone examples in Python.

## Prerequisites
* A Keystone instance (for instance, inside a Docker container)
* Python packages _python-keystoneclient_ and _keystoneauth1_ on your local machine. You can install them with pip: 

	````bash
	$ pip install python-keystoneclient keystoneauth1
	```` 

## retrieve_token.py
Authenticates with Keystone through username+password credentials of a test user. Extracts its authentication token (signed PKI token in CMS format) and writes it to a file at a desired location.

````bash
$ python retrieve_token.py -h       # Shows help
$ python retrieve_token.py myToken  # Writes raw Keystone token to a file myToken
```` 

## verify_token.py
Demonstrates offline verification of PKI token, previously generated as a result of script _retrieve_token.py_. Upon success, prints decoded token data, which can be used later on.

Note that for offline verification of tokens we don't need to offload this work to Keystone, but we do need Keystone's CA and signing certificates. If we don't have a local copy of these two, we must obtain it through Keystone API.

````bash
$ python verify_token.py -h  # Shows help
$ python verify_token.py keystone-ca.crt keystone-signing.crt myToken # Reads (fetches and saves if not present) local copies of Keystone's certificates and verifies token written in a given file
```` 
