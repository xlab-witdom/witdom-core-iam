import argparse
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

parser = argparse.ArgumentParser(description='Retrieving a Keystone PKI token for an authenticated user')
parser.add_argument('token_file', type=str, metavar='CMS-Token-path', help="Path where PKI token in CMS format is to be saved")

ip_keystone = "localhost"
public_endpoint = 'http://%s:5000/v3' % (ip_keystone)

# Authenticate with u+pw credentials to retrieve a token
auth = v3.Password( auth_url=public_endpoint,\
				    username='testuser',\
				    password='testuser',\
				    user_domain_id='default')
sess = session.Session(auth=auth)
keystone = client.Client(session=sess)

# Retrieve authentication info that includes token
access_info = keystone.get_raw_token_from_identity_service(public_endpoint, username='admin', password='adminpw' , user_domain_id="default")

# Extract authentication token 
token = access_info.auth_token

# Save token to a file
args = parser.parse_args()
token_file = args.token_file
with open(token_file, 'w') as f:
	f.write(token)
	f.close()


