import argparse
import sys
import os.path
import textwrap
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client
from keystoneclient.common import cms
from keystoneclient.exceptions import CMSError

# Writes string content to file
def save_file(filename, content):
	with open(filename, 'w') as f:
		f.write(content)
		f.close()

# If communication with keystone is necessary (when fetching certifiates), client object is returned
def get_client(username, pw):
	# Authenticate with u+pw credentials and return client object
	auth = v3.Password(auth_url="http://localhost:5000/v3", username=username, password=pw, user_domain_id='default')
	sess = session.Session(auth=auth)
	return client.Client(session=sess)

# If local copies of signing and CA certificates don't exist, get them from keystone
#	and save them to a file
def check_certs(signing_crt, ca_crt):
	keystone = get_client("admin", "adminpw")
	if not os.path.isfile(signing_crt):
		print "Local copy of keystone signing certificate doesn't exist, fetching it and storing to {}".format(signing_crt)
		save_file(signing_crt, keystone.simple_cert.get_certificates())

	if not os.path.isfile(ca_crt):
		print "Local copy of keystone CA certificate doesn't exist, fetching it and storing to {}".format(ca_crt)
		save_file(ca_crt, keystone.simple_cert.get_ca_certificates())

# Returns a valid Base64 encoded string so that it can be correctly decoded
def correct_cms_content(content):
	content = content.replace('-', '/')	# Keystone uses non-standard Base64 version that uses '-' instead of dashes
	content = '\n'.join(textwrap.wrap(content, 64))	# Newline every 64 chars
	content = "{}\n{}\n{}".format("-----BEGIN CMS-----", content, "-----END CMS-----") 
	return content

# Process command line arguments
parser = argparse.ArgumentParser(description='Verification of Keystone PKI tokens')
parser.add_argument('ca_crt', metavar='CA-cert-path', type=str, help="Path to local copy of Keystone's CA certificate")
parser.add_argument('signing_crt', metavar='Signing-cert-path', type=str, help="Path to local copy of Keystone's CA certificate")
parser.add_argument('token', type=str, metavar='CMS-Token-path', help="Path to signed PKI token in CMS format to be verified")

args = parser.parse_args()

signing_cert_file = args.signing_crt
ca_cert_file = args.ca_crt
token_file = args.token

# Check whether certificates exist paths provided as command line args
check_certs(signing_cert_file, ca_cert_file)

# Check whether PKI token exists at a given path
if not os.path.isfile(token_file):
	print "Cannot start verification process - file with token in CMS format doesn't exist."
	sys.exit(1)

# Token file exists, read its content and try to verify it
try:
	with open(token_file, 'r') as f:
		cms_str = f.read();
		f.close();

	corrected_cms_str = correct_cms_content(cms_str)
	decoded_data = cms.cms_verify(corrected_cms_str, signing_cert_file, ca_cert_file)
	print "Successfully verified token, decoded data:\n{}".format(decoded_data)
except CMSError as e:
	print "Keystone client CMSError: {}".format(str(e))
	sys.exit(1)
