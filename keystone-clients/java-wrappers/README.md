# Java Client for retrieving authentication token from Keystone
This project wraps code for interacting with OpenStack Keystone for obtaining authentication token in an easy way. It
also contains helper procedures for extracting user information from Keystone tokens.

## Prerequisites
* A Keystone instance (for instance, inside a Docker container)
* Apache Maven for building project
* A slightly modified copy of [OpenStack Java SDK](https://github.com/mancabizjak/openstack-java-sdk) (follow installation instructions there). This fork of OpenStack Java SDK adds some capabilities for interacting with Keystone's Token resource.

## Installation
*Note: prior to installation, ensure that maven project OpenStack Java SDK (see prerequisites) is installed.*
1. Clone this repository to an arbitrary location
2. `cd` to the Java project's root directory (where _pom.xml_ is located)
3. Run `mvn install` which will build this project
4. Run `mvn test` to compile and run tests with a mocked Keystone component and a fixed token

## Modifying source code
- If you wish to modify these source files, run `mvn compile` from the root directory 
- If you modify testing code, just run `mvn test` to compile and run unit tests

## How to use Java Client and procedure for extracting user data from a Keystone token
Please see the examples [here](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone-clients/integration-tests/java/?at=master)
