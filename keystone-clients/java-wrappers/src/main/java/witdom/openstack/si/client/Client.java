package witdom.openstack.si.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import com.sun.jersey.api.client.ClientHandlerException;
import com.woorea.openstack.base.client.OpenStackResponse;
import com.woorea.openstack.keystone.v3.Keystone;
import com.woorea.openstack.keystone.v3.model.Authentication;
import com.woorea.openstack.keystone.v3.model.Authentication.Identity;
import witdom.openstack.si.config.KeystoneConfiguration;

/* *** Java client ***
   * Provides username + password credentials in order to retrieve authentication token from Keystone
*/
public class Client
{
	private KeystoneConfiguration keystoneConf;
	private Keystone keystone;

	public Client(KeystoneConfiguration keystoneConf) {
		this.keystoneConf = keystoneConf;
		this.keystone = new Keystone(this.keystoneConf.getv3PublicEndpoint()); 	// Create keystone client instance
	}

	/* For easier mocking */
	public void setKeystone(Keystone keystone) {
		this.keystone = keystone;
	}

	/* Retrieves authentication token from Keystone
         * Expects that user with provided username and password exists in Keystone in the default domain */
	public String authenticateWithUsernamePassword(String username, String password)
	{
		Authentication auth = new Authentication();
		auth.setIdentity(Identity.password("default", username, password)); // This user must exist in Keystone

		String token = null;

		try {
			OpenStackResponse response = this.keystone.tokens().authenticate(auth).request();
			token = response.header("X-Subject-Token");
		} catch (ClientHandlerException e) {
			//e.printStackTrace();
			System.out.println("Cannot connect to keystone - please check if Keystone instance is up and running");
			System.exit(1);
		}

		return token;
	}
}