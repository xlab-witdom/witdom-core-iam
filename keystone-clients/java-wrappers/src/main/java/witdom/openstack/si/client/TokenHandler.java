package witdom.openstack.si.client;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.bc.BcRSASignerInfoVerifierBuilder;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Collection;

public class TokenHandler
{
    private JSONObject userData;

    public TokenHandler(String token)
    {
        String base64ConformToken = token.replaceAll("-", "/");
        byte[] data = Base64.decodeBase64(base64ConformToken);
        JSONObject json = extractJSONFromToken(data);
        this.userData = extractUserDataFromJSON(json);
    }


    public String getUserId()
    {
        return getUserDataField("id");
    }

    public String getUserName()
    {
        return getUserDataField("name");
    }


    /* Extracts a value from the field "field" that is in "user" JSON data */
    String getUserDataField(String field)
    {
        try {
            return this.userData.getString(field);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    /* Extracts the "user" portion of the JSON */
    JSONObject extractUserDataFromJSON(JSONObject json)
    {
        try {
            JSONObject tokenData = json.getJSONObject("token");
            JSONObject userData = tokenData.getJSONObject("user");
            return userData;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /* Extracts JSON information encoded in the token
     * after it has been put through Base64 decoding */
    JSONObject extractJSONFromToken(byte[] data)
    {
        try {
            CMSSignedData csd = getSignedData(data);
            String tokenContent = getTokenContent(csd);

            if (tokenContent != null) { // Token content was extracted successfully
                return new JSONObject(tokenContent);

            } else {
                System.err.println("Could not determine token content, got NULL instead.");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /* Gets signed data from the PKI token */
    CMSSignedData getSignedData(byte[] data) {
        CMSSignedData signed = null;
        try {
            signed = new CMSSignedData(data);
        } catch (CMSException e) {
            e.printStackTrace();
        }

        return signed;
    }

    /* Gets plaintext content from signed data as a string
     * The returned string can be then further parsed as JSON */
    String getTokenContent(CMSSignedData signedData)
    {
        Object content = signedData.getSignedContent().getContent();
        if (content instanceof byte[])
        {
            String contentStr = new String((byte[]) content);
            return contentStr;
        }
        return null;
    }

}
