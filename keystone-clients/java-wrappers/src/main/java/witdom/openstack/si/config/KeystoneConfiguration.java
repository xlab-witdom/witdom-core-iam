package witdom.openstack.si.config;


public class KeystoneConfiguration
{

    public static final String LOCALHOST = "127.0.0.1";
    public static final int DEFAULT_ADMIN_PORT = 35357;
    public static final int DEFAULT_PUBLIC_PORT = 5000;

    private String ip;
    private int admin_port;
    private int public_port;

    public KeystoneConfiguration() {
        this.ip = LOCALHOST;
        this.admin_port = DEFAULT_ADMIN_PORT;
        this.public_port = DEFAULT_PUBLIC_PORT;
    }

    public KeystoneConfiguration(String ip) {
        this.ip = ip;
        this.admin_port = DEFAULT_ADMIN_PORT;
        this.public_port = DEFAULT_PUBLIC_PORT;
    }

    public KeystoneConfiguration(String ip, int publicPort, int adminPort) {
        this.ip = ip;
        this.admin_port = publicPort;
        this.public_port = adminPort;
    }

    public String getIp() {
        return ip;
    }

    public int getAdmin_port() {
        return admin_port;
    }

    public int getPublic_port() {
        return public_port;
    }


    public String getv3AdminEndpoint() {
        return String.format("http://%s:%d/v3", this.ip, this.admin_port);
    }

    public String getv3PublicEndpoint() {
        return String.format("http://%s:%d/v3", this.ip, this.public_port);
    }

}
