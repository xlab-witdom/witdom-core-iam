package witdom.openstack.si;

import com.woorea.openstack.base.client.OpenStackResponse;
import com.woorea.openstack.keystone.v3.Keystone;
import com.woorea.openstack.keystone.v3.api.TokensResource;
import com.woorea.openstack.keystone.v3.model.Authentication;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import witdom.openstack.si.client.Client;
import witdom.openstack.si.config.KeystoneConfiguration;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ClientTest extends TestCase
{
    Client client;

    @Mock
    Keystone mockKeystone;

    @Mock
    Authentication mockAuth;

    @Mock
    TokensResource.Authenticate mockAuthenticate;

    @Mock
    TokensResource mockTokensResource;

    @Mock
    OpenStackResponse mockResponse;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        KeystoneConfiguration keystoneConf = new KeystoneConfiguration();
        client = new Client(keystoneConf);
        client.setKeystone(mockKeystone);
    }


    @Test
    public void testRetrievingAuthToken()
    {
        String username = "test";
        String password = "test";

        when(mockResponse.header("X-Subject-Token")).thenReturn("MIICyAYJKoZIhvcNAQcCoIICuTCCArUCAQExDTALBglghkgBZQMEAgEwggEWBgkqhkiG9w0BBwGgggEHBIIBA3sidG9rZW4iOiB7Imlzc3VlZF9hdCI6ICIyMDE2LTEwLTEzVDA5OjQyOjQ1LjEwMjA0MVoiLCAiZXh0cmFzIjoge30sICJtZXRob2RzIjogWyJwYXNzd29yZCJdLCAiZXhwaXJlc19hdCI6ICIyMDE2LTEwLTEzVDEwOjQyOjQ1LjEwMTk2NVoiLCAidXNlciI6IHsiZG9tYWluIjogeyJpZCI6ICJkZWZhdWx0IiwgIm5hbWUiOiAiRGVmYXVsdCJ9LCAiaWQiOiAiYTRlYWY1YmNiZmJlNGRhODkzYTY3MDdmYTU4NTM5N2UiLCAibmFtZSI6ICJ0ZXN0dXNlciJ9fX0xggGFMIIBgQIBATBcMFcxCzAJBgNVBAYTAlVTMQ4wDAYDVQQIDAVVbnNldDEOMAwGA1UEBwwFVW5zZXQxDjAMBgNVBAoMBVVuc2V0MRgwFgYDVQQDDA93d3cuZXhhbXBsZS5jb20CAQEwCwYJYIZIAWUDBAIBMA0GCSqGSIb3DQEBAQUABIIBABaY5SAmHs2I6uX3UMTPCJTP4kfd738hvw18LmjpFfKvk-G14vs+03awstXBLvCkXfl2ECU4pcvEbh7-NQiXEZrW6RapfxgQpRmYeZJOI2JwpYey9AwHA7LRpJq027+-jPSeBk9y+utajvZG5ZSC3Klr-V00s6smaUTn-hUADs0-8JqtRuH1e+NC3rVFnXK-67MOwuMc5ibv9H1Jr3-duFLLhBTbcPBF1wM2U-TBs+w5s7jqINeVMwihqwFw6ZdL9KNENwSTgQMtozmQ9sIlpkwxFm8MepZVbiAr7bmBcTtQvTPHjnfDsnMtT8ztDKa44p0C2YG-1+CmUjc-xLUmC6M=");
        when(mockAuthenticate.request()).thenReturn(mockResponse);
        when(mockTokensResource.authenticate(Mockito.isA(Authentication.class))).thenReturn(mockAuthenticate);
        when(mockKeystone.tokens()).thenReturn(mockTokensResource);

        String authToken = client.authenticateWithUsernamePassword(username, password);
        assertNotNull(authToken);
    }

}