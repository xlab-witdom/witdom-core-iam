package witdom.openstack.si;


import org.junit.Test;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import witdom.openstack.si.client.TokenHandler;

public class TokenHandlerTest
{
    static TokenHandler testTokenHandler;


    @Test
    public void testExtractUserNameFromToken() throws FileNotFoundException
    {
        String testTokenFile = getClass().getResource("/token.txt").getFile();
        String testToken = new Scanner(new File(testTokenFile)).useDelimiter("\\Z").next();
        String userName = new TokenHandler(testToken).getUserName();
        assertEquals(userName, "admin");
    }

    @Test
    public void testExtractUserIdFromToken() throws FileNotFoundException
    {
        String testTokenFile = getClass().getResource("/token.txt").getFile();
        String testToken = new Scanner(new File(testTokenFile)).useDelimiter("\\Z").next();
        String userId = new TokenHandler(testToken).getUserId();
        assertEquals(userId, "8262f3e7251d4c50841389d01ff477b5");
    }

    @Test(expected = NullPointerException.class)
    public void testWithInvalidToken() {
        String testToken = "String that is not in CMS format";
        new TokenHandler(testToken).getUserId();
    }
}
