# JavaScript Client wrapper for management of Keystone authentication tokens
This project contains a simple NodeJS module that wraps the code for interacting with OpenStack Keystone for easier authentication token retrieval and validation. It depends on a slightly modified version of [node-openstack-wrapper](https://github.com/godaddy/node-openstack-wrapper).

The wrapper exposes a TokenValidationService object with functions:
* **getAuthToken(username, pw, callback)** for retrieval of an authentication token based on a username and password,
* **validateAuthenticationToken(tokenToValidate, adminToken, callback)** for validation of authentication tokens, which can be requested by administrative users (their authentication tokens indicate elevated privileges). 

Both functions also propagate possible Keystone errors through callbacks.

## Prerequisites
* NodeJS
* npm

## Installation
Run

````bash
$ npm install node-openstack-token-utils
```` 

where _node-openstack-token-utils_ corresponds to the folder containing code for JavaScript Client wrapper. This will install NodeJS module _openstack-token-utils_ inside the _node_modules_ directory.

## Using JavaScript Keystone Client wrapper in your code
Please see the example [here](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone-clients/integration-tests/javascript/?at=master)
