var auth = require('openstack-token-utils');

/* Configure access to IAM service */
keystoneAdminUser = "admin";
keystoneAdminPass = "adminpw";
protocol = "http";
keystoneURL = "172.16.95.136"; // 127.0.0.1
keystonePort = 5000;
keystoneEndpoint = protocol + "://" + keystoneURL + ":" + keystonePort + "/v3";


authService = new auth.TokenValidationService(keystoneEndpoint, keystoneAdminUser, keystoneAdminPass);

/* Authentication token, forwarded to broker by application */
/* TODO Modify this */
/*   This value will be retrieved from application in production */
/*   In order to retrieve a token for testing purposes,  */
var appToken = null;

testValidation();

function testValidation() {
    authService.getAuthToken("testuser", "testuser", function(token) {
        appToken = token;
        console.log("Verifying token", appToken);
        authService.validateAuthenticationToken(appToken, function(tokenValid) {
            console.log("Result", tokenValid);
            /*if (tokenValid != null) {
                console.log("Token validity:", tokenValid);
                /*if (tokenValid) {
                    console.log("Expiration:", body.token.expires_at);
                }*/
            //}
        });
    });
}

/* Performs validation of authentication token */
//authService.validateAuthenticationToken(appToken);