var openstack_wrapper = require('openstack-wrapper');


function TokenValidationService(keystone_endpoint, adminUser, adminPass)
{
    /* Keystone REST client instance */ 
    this.keystone = new openstack_wrapper.Keystone(keystone_endpoint);
    console.log("Using Keystone service at", keystone_endpoint);
    
    this.adminUsername = adminUser;
    this.adminPassword = adminPass;
    
    /* Token of an entity with administrative privileges (broker) */
    this.adminToken = null;
}


/* Validates provided authentication token after administrative token is retrieved */
TokenValidationService.prototype.validateAuthenticationToken = function(tokenToValidate, fn) {
    var self = this;
    var tokenValid = null;
    self.getAuthToken(self.adminUsername, self.adminPassword, function(token) {
        self.adminToken = token;  
        console.log("Admin token", self.adminToken);
        self.keystone.checkToken(self.adminToken, tokenToValidate, function(error, body) {
            if(error) {
                console.error('An error occured while retrieving token', error.status, error.message);
            } else {
                console.log('No error');
            }

            tokenValid =  body.token_valid;
            //console.log(tokenValid);
            fn(tokenValid);
        });           
    });
};  

TokenValidationService.prototype.getAuthToken = function(username, pw, fn) {
 this.keystone.getToken(username, pw, function(error, token) {
        if(error) {
            console.error('An error occured while retrieving token', error);
        } else {
            fn(token.token);
        }
    });   
};


module.exports = TokenValidationService;

