name 'keystone'
description 'Cookbook for setting up OpenStack Keystone'
version '1.0.0'

maintainer 'Manca Bizjak'
maintainer_email 'manca.bizjak@xlab.si'

depends 'mysql2_chef_gem'
depends 'database'