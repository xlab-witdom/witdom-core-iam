# keystone::default_modified (modified to run smoothly with Cloudify deployment)
# Downloads and installs keystone with default configuration
# Downloads and installs keystone python client

execute 'Updating packages' do
	command 'sudo apt-get update'
	ignore_failure true
end

package ["keystone", "python-pip"]

bash 'Installing python packages' do
	code <<-EOH
		sudo pip install lxml pytz
		sudo pip install --upgrade pbr
	EOH
end

execute 'Installing Openstack client' do
	command 'sudo pip install python-openstackclient python-keystoneclient==3.1.0 wrapt rfc3986'
	ignore_failure true
end

####################
# Configure Keystone
####################
# Exclude port where Keystone services run from available system ports
file '/etc/sysctl.d/keystone.conf' do
	content 'net.ipv4.ip_local_reserved_ports = 35357'
	mode '0755'
end

# Reload system configuration to apply above changes
execute 'Reloading system configuration' do
	command 'sysctl --system'
end


