service 'keystone' do
	action :restart
end

# Generate certificates and keys 
execute 'Configuring Keystone to use PKI authentication tokens' do
	command "keystone-manage pki_setup --keystone-user #{node['keystone']['user']} --keystone-group #{node['keystone']['user']}"
	retries 5
end

# TODO Move this configuration to attributes
template "/home/#{node['keystone']['user']}/initialize_data.py" do 
	source 'initialize_data.erb'
	mode '0777'
	variables({
		:admin_token => node['keystone']['admin_token'],
		:admin_pw => node['keystone']['admin_pw'],
		:barbican_pw => node['barbican']['pw'],
        :barbican_ip => node['barbican']['ip']
	})
end

# Script that is called here may initially fail due to keystone not being
#	accessible yet
execute 'Starting data initialization script' do
	cwd "/home/#{node['keystone']['user']}"
	command 'python initialize_data.py'
	retries 3
	retry_delay 5
end