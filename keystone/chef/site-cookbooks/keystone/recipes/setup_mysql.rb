# keystone::setup_mysql
# Sets up MySQL Database and MySQL user for keystone, runs MySQL services

package "python-mysqldb"

# Creates MySQL client
mysql_client 'default' do
	action :create
end

# Installs mysql2 ruby gem
mysql2_chef_gem 'default' do
	action :install
end

# sometimes fails
mysql_service 'keystone_service' do
	port '3306'
	#version '5.5'
	initial_root_password 'keystone_root'
	action [:create, :start]
end

mysql_connection_info  = {
	:host => '127.0.0.1',
    :username => 'root',
    :password => 'keystone_root'
}

# Creates new database
mysql_database 'keystone_db' do
	connection mysql_connection_info
	action :create
end

# Creates a new database user.
mysql_database_user 'keystone_admin' do
	connection mysql_connection_info
	password 'keystone_admin_pw'
	action [:create, :grant]
end

template '/etc/keystone/keystone.conf' do #'/home/vagrant/keystone/etc/keystone.conf'
	source 'keystone.erb'
	mode '0755'
	variables({
		:connection_string => "mysql://keystone_admin:keystone_admin_pw@127.0.0.1/keystone_db",
		:token_expiration_period => node['keystone']['token_expiration_period'],
		:admin_token => node['keystone']['admin_token']
	})
end

execute 'Removing default sqlite database file' do
	command 'rm -f /var/lib/keystone/keystone.db'
end

# Fails
execute 'Creating Keystone database schema' do
	command 'keystone-manage db_sync'
	retries 2
	ignore_failure true
end