# WITDOM IAM - Keystone
This document describes basic usage of OpenStack IAM Service - Keystone, which is started inside a docker container and can be used for local development.

### Files included
* _Dockerfile_ - used to build the docker image
* _conf/keystone.conf.template_ - main Keystone configuration file template. In this file, string _{ip}_ will be replaced prior to building Docker image in order to determine correct public and admin endpoints for Keystone.
* _get_docker_ip.sh_ - A simple bash script that determines IP address of docker0 network interface. Is called prior to building Docker container. Its otuput is used to replace configuration in _keystone.conf.template_.
* _conf/initialize.sh_ - Bash script for initialization of Keystone data. Used to configure some projects, users, services, endpoints and roles.
* _start.sh_ - Startup script that sets up Keystone, runs Keystone service and starts data initiaization script

## Configuration
There is a Makefile that can be modified in order to change some basic configuration like port mapping, container and image names.

## Building
Run `make`. This will start the building process and will run the container after successfull build. Container is run in a detached mode, meaning that there won't be any output from Keystone after container is run.

## See container logs
To see Keystone logs, run `make logs`.

## Connect to Docker container
Run `make bash`.

## Cleanup
Run `make clean` to stop and remove Docker container. 