#!/bin/bash

CONTROLLER=${IP:-"127.0.0.1"}

SWIFT_HOST=${SWIFT_IP:-CONTROLLER}
KM_HOST=${SWIFT_IP:-CONTROLLER}

ADMIN_PORT=${ADMIN_PORT:-35357}
PUBLIC_PORT=${PUBLIC_PORT:-5000}

export OS_TOKEN=ADMIN
export OS_IDENTITY_API_VERSION=3
export OS_URL=http://$CONTROLLER:$ADMIN_PORT/v3

function get_id() {
  echo `"$@" | sed -n '6p' | cut -d '|' -f3`
}

echo "Creating projects (tenants)"
openstack project create admin    # OK
openstack project create service  # OK
openstack project create witdom   # OK

echo "Creating users"
openstack user create admin --password adminpw --project admin
openstack user create swift --password swiftpw --project service
openstack user create barbican --password barbicanpw --project service
openstack user create testuser --password testuser --project witdom 

echo "Configuring roles"
openstack role create admin   # OK
openstack role add admin --domain default  --user admin
openstack role add admin --project admin   --user admin
openstack role add admin --project service --user swift
openstack role add admin --project service --user barbican

echo "Configuring services"
# We must extract IDs of created services, so we can pass them when creating endpoints
IDENTITY_SERVICE_ID=$(get_id openstack service create identity \
  --name keystone \
    --description "Keystone Identity Service")

SWIFT_SERVICE_ID=$(get_id openstack service create object-store \
  --name swift \
    --description "Swift Service")

BARBICAN_SERVICE_ID=$(get_id openstack service create key-manager \
  --name barbican \
    --description "Barbican Service")

echo "Configuring Endpoints"
openstack endpoint create \
  $IDENTITY_SERVICE_ID admin http://$CONTROLLER:$ADMIN_PORT/v3 \
  --region RegionOne

openstack endpoint create \
  $IDENTITY_SERVICE_ID public http://$CONTROLLER:$PUBLIC_PORT/v3 \
  --region RegionOne

openstack endpoint create \
  $BARBICAN_SERVICE_ID public http://$KM_HOST:9311/v1 \
  --region RegionOne

openstack endpoint create \
  $SWIFT_SERVICE_ID public "http://$SWIFT_HOST:8080/v1/AUTH_\$(tenant_id)s" \
  --region RegionOne

openstack endpoint create \
  $SWIFT_SERVICE_ID internal "http://$SWIFT_HOST:8080/v1/AUTH_\$(tenant_id)s" \
  --region RegionOne

openstack endpoint create \
  $SWIFT_SERVICE_ID admin "http://$SWIFT_HOST:8080/" \
  --region RegionOne