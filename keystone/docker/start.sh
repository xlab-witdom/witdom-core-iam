#!/bin/bash

# Set up keystone database, database user and its privileges
service mysql start
mysql -u root -e "CREATE USER 'keystone'@'localhost' IDENTIFIED BY 'keystone_admin_pw'" 
mysql -u root -e "CREATE DATABASE keystone" 
mysql -u root -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost'" 
rm -f /var/lib/keystone/keystone.db 

# Create appropriate tables in the database
keystone-manage db_sync

# Generate certificates and keys 
keystone-manage pki_setup \
 --keystone-user root \
 --keystone-group root

# Start appropriate services and initialize data for keystone
nohup keystone-all > keystone.out 2> keystone.err < /dev/null & 

# Wait until keystone services are started before continuing (otherwise initialization script can fail)
echo "Waiting for keystone services to start properly"
sleep 10 
echo "Keystone started"

# Run initialization script
./root/initialize.sh && echo "Keystone data initialized"

# Restart keystone, run it in foreground to keep container running
killall keystone-all
keystone-all --verbose